### ReactJs + Ethereum + Truffle bloggomg app

Download and run Ganache 

https://trufflesuite.com/ganache/

npm i -g truffle 

go to backend folder
```bash
$ truffle migrate --reset 
```
copy contract json file to client/src/PostContract.json 

copy contract address from truffle log to client/src/config.js

go to client folder
```bash
$ npm install
$ npm run start
```
