import React from "react"; 
import Navbar from "./Navbar";
import Post from "./Post";

type theProps = {
    posts: any;
    title: string;
    img: string;
    text: string;
    setTitle: any;
    setImg: any;
    setText: any;
    addPost: any;
    deletePost: any;
  };

const Blog: React.FunctionComponent<theProps> = (props) => {
  
    const { posts, title, img, text, setTitle, setImg, setText, addPost, deletePost } = props;

 //   console.log(posts)

    return (
        <div className="container-flui">

          <Navbar />

<div className="col-md-12 addpost">

<div className="mb-3">
  <label htmlFor="exampleFormControlInput1" className="form-label">Post title</label>
  <input
  value={title}
  onChange={e => setTitle(e.target.value)}
  type="text" className="form-control"  placeholder="Enter Post title" />
</div> 

<div className="mb-3">
  <label htmlFor="exampleFormControlInput1" className="form-label">Post image URL</label>
  <input
  value={img}
  onChange={e => setImg(e.target.value)}
  type="text" className="form-control" placeholder="https://website.com/image.jpg" />
</div>

<div className="mb-3">
  <label htmlFor="exampleFormControlTextarea1" className="form-label">Post text</label>
  <textarea
  value={text}
  onChange={e => setText(e.target.value)}
  className="form-control" rows={3}></textarea>
</div>

<button
onClick={addPost}
className="btn btn-primary">Add a Post</button>


<h2 className="text-center">My posts</h2>

    

</div>
<div className="col-md-12">
    <div className="row">
    { posts && posts.map((post: any) => (
     
     <Post 
     key={post.id}
     post={post}
     />
 ))
 }
    </div>
</div>




        </div>
    );

}

export default Blog;