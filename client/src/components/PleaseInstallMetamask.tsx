import React from "react";

function PleaseInstallMetamask() {

    return (
        <div className="container-fluid">
            <h1 className="text-center">You need to install ethereum metamask wallet to be able to use this website</h1>
        </div>
    )

}

export default PleaseInstallMetamask;