import React, {useState, useEffect} from 'react';

// import { MetaMaskInpageProvider } from "@metamask/providers";
import { ethers } from 'ethers';

import { PostContractAddress } from './config.js'
import PostAbi from './PostContract.json'
import Wallet from './components/Wallet';
import Blog from './components/Blog';
import PleaseInstallMetamask from './components/PleaseInstallMetamask';


// declare global {
//   interface Window{
//     ethereum?:MetaMaskInpageProvider
//   }
// }

declare global {
  interface Window{
    ethereum?:any
  }
}

function App() { 

  const [correctNetwork, setCorreccNetwork] = useState(false)
  const [isUserLoggedIn, setIsUserLoggedIn] = useState(false)
  const [currentAccount, setCurrentAccount] = useState('')

  const [input, setInput] = useState('')

  const [title, setTitle] = useState('')
  const [img, setImg] = useState('')
  const [text, setText] = useState('')

  const [posts, setPosts] = useState<any>([])

  useEffect(() => {
    connectWallet()
    getAllPosts()
  }, [])

  // Calls Metamask to connect wallet on clicking Connect Wallet button
  const connectWallet = async () => {
    try {
      const { ethereum } = window 
      if (!ethereum) {
        console.log("MetaMask not detected! ")
        alert("Please install MetaMask to use this app!")
        return
      }
      let chainId = await ethereum.request({ method: "eth_chainId" })
      console.log("Connected to chain: ", chainId) 

      // const rinkebyChainId = '0x4'
      // if (chainId !== rinkebyChainId) {
      //   console.log("You are not connected to Rinkedy")
      //   setCorreccNetwork(true)
      //   return
      // } else {
//   setCorreccNetwork(true)
    //  }

      setCorreccNetwork(true)

      const accounts: any = await ethereum.request({ method: 'eth_requestAccounts' }) 

      console.log("Found account: ", accounts[0])
      setIsUserLoggedIn(true)
      setCurrentAccount(accounts[0])

    } catch (error) {
      console.log(error)
    }
  }

  // Just gets all the tasks from the contract
  const getAllPosts = async () => {
    try {
      const {ethereum} = window
      if (ethereum) {
        const provider = new ethers.providers.Web3Provider(ethereum)
        const signer = provider.getSigner()
        const PostContract = new ethers.Contract(
          PostContractAddress,
          PostAbi.abi,
          signer
        ) 
        let allPosts = await PostContract.getMyPosts()
        setPosts(allPosts)
      } else {
        console.log("Ethereum object does not exist")
      }
    } catch (error) {
      console.log(error)
    }
  }

  // Add tasks from front-end onto the blockchain
  const addPost = async (e: any) => {
    e.preventDefault() 

    let post = {
      postTitle: title,
      postImage: img,
      postText: text,
      isDeleted: false,
    }
console.log(post)
    try {
      const {ethereum} = window
      if (ethereum) {
        const provider = new ethers.providers.Web3Provider(ethereum)
        const signer = provider.getSigner()
        const PostContract = new ethers.Contract(
          PostContractAddress,
          PostAbi.abi,
          signer
        ) 

        PostContract.addPost(post.postTitle, post.postImage, post.postText, post.isDeleted)
        .then((res: any) => {
          setPosts([...posts, post])
          console.log("Added task")
        })
        .catch((err: any) => {
          console.log(err)
        })
      } else {
        console.log("Ethereum object does not exist")
      }
    } catch (error) {
      console.log(error)
    }

    setTitle('')
    setImg('')
    setText('')

  }

  // Remove tasks from front-end by filtering it out on our "back-end" / blockchain smart contract
  const deletePost = (key: any) => async () => {
    try {
      const {ethereum} = window
      if (ethereum) {
        const provider = new ethers.providers.Web3Provider(ethereum)
        const signer = provider.getSigner()
        const PostContract = new ethers.Contract(
          PostContractAddress,
          PostAbi.abi,
          signer
        )   
        const deleteTaskTx = await PostContract.deleteTask(key, true)
        console.log("Successfully deleted", deleteTaskTx)
        let allPosts = await PostContract.getMyPosts();
        setPosts(allPosts)
      } else {
        console.log("Ethereum object does not exist")
      }
      
    } catch (error) {
      console.log(error)
    }
  }


  // if (window.ethereum === 'undefined') {
  //   return <PleaseInstallMetamask />
  // }

  if (!window.ethereum) {
    return <PleaseInstallMetamask />
  
  } else if (window.ethereum && !isUserLoggedIn) {
    return <Wallet />
  } else {
    return <Blog 
    posts={posts} title={title} img={img} text={text} setTitle={setTitle} setImg={setImg} setText={setText} addPost={addPost} deletePost={deletePost}
    />
  }




  // return (
  //   { isUserLoggedIn ? <Wallet connectWallet={connectWallet} /> : <Blog />
  // }
  // );
}

export default App;
